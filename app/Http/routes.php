<?php

/* ... */

use App\Note;

Route::get('/', function () {
    return view('welcome');
});


Route::get('notes', 'NotesController@index');
Route::get('notes/create', 'NotesController@create');
// 3r video, minuto 3
Route::post('notes', 'NotesController@store');

Route::get('notes/{note}', 'NotesController@show')->where('note', '[0-9]+');