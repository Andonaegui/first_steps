<?php

use App\Note;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NoteExerciseTest extends TestCase
{
	use DatabaseTransactions;

    public function test_notes_summary_and_notes_details()
    {
    	$text = 'Begin of the note. Ipsum cupiditate sint et suscipit sit qui tempore. Distinctio adipisci autem minima molestiae vel voluptas. Soluta minima quia iusto voluptatum aliquam eaque. Accusamus qui illo ducimus dolorem labore qui perferendis.';
    	$text .= 'End of the note';

    	$note = Note::create(['note' => $text]);

        $this->visit('notes')
        	->see('Begin of the note')
        	->seeInElement('.label','Others')
        	->dontSee('End of the note')
        	->seeLink('View note')
        	->click('View note')
        	->see($text)
        	->seeLink('View all notes', 'notes');
    }
}
